export const SUPPORTED_LOCALES = ["de", "en"];
export const DEFAULT_LOCALE = "de";
export const LOCAL_STORAGE_LOCALE_KEY = "locale";
