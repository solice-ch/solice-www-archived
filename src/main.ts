import Vue from "vue";
import App from "./App.vue";
import getI18n from "./i18n";
import getRouter from "./router";
import VueCompositionApi from "@vue/composition-api";
import { createStore } from "./store/store";

Vue.config.productionTip = false;
Vue.use(VueCompositionApi);

const storeContainer = createStore(localStorage);
export const i18n = getI18n(storeContainer.store);

new Vue({
  router: getRouter(storeContainer.store),
  i18n,
  render: h =>
    h(App, {
      props: {
        window,
        document,
        store: storeContainer.store,
        mutations: storeContainer.mutations,
        i18n
      }
    })
}).$mount("#app");
