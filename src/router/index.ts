import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import { SUPPORTED_LOCALES } from "../constants/locales";
import RouteLocalizer from "./route-localizer";
import { Store } from "../store/store";

if (process?.env?.NODE_ENV !== "test") {
  Vue.use(VueRouter);
}
const localizeRoutes = new RouteLocalizer(SUPPORTED_LOCALES);
Vue.prototype.$localizeRoutes = localizeRoutes;

const routes: Array<RouteConfig> = [
  {
    path: `/:locale${localizeRoutes.getLocaleRegexString()}`,
    component: {
      render(c) {
        return c("router-view");
      }
    },
    children: [
      {
        path: "",
        name: "Home",
        component: Home
      },
      {
        path: "about",
        name: "About",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/About.vue")
      },
      {
        path: "contact",
        name: "Contact",
        component: () =>
          import(/* webpackChunkName: "contact" */ "../views/Contact.vue")
      },
      {
        path: "webapps",
        name: "Websites and Apps",
        component: () =>
          import(/* webpackChunkName: "webapps" */ "../views/Webapps.vue"),
        meta: {
          sidebar: true
        }
      },
      {
        path: "cms",
        name: "Content Management System",
        component: () =>
          import(/* webpackChunkName: "cms" */ "../views/Cms.vue"),
        meta: {
          sidebar: true
        }
      },
      {
        path: "individual",
        name: "Individualsoftware",
        component: () =>
          import(/* webpackChunkName: "cms" */ "../views/Individual.vue"),
        meta: {
          sidebar: true
        }
      }
    ]
  }
];
export const getRouter = (store: Store): VueRouter => {
  const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
  });

  router.beforeEach((to, _, next) => {
    if (!localizeRoutes.isLocalized(to)) {
      next({
        path: localizeRoutes.getFullLocalizedPath(to, store.currentLocale)
      });
    } else {
      next();
    }
  });

  return router;
};

export default getRouter;
