import { Route } from "vue-router";

export default class RouteLocalizer {
  private readonly supportedLocales: string[];
  private static readonly notMatchable: string = "(?=a)b";
  constructor(supportedLocales: string[]) {
    this.supportedLocales = supportedLocales;
  }

  getLocaleRegexString(): string {
    if (this.supportedLocales.length) {
      return `(${this.supportedLocales.join("|")})`;
    }
    return RouteLocalizer.notMatchable;
  }

  isLocalized(
    route: Route | string,
    supportedLocales: string[] = this.supportedLocales
  ): boolean {
    return typeof route === "object"
      ? this.isLocalizedString((route as Route).fullPath, supportedLocales)
      : this.isLocalizedString(route, supportedLocales);
  }

  private isLocalizedString(
    path: string,
    supportedLocales: string[] = this.supportedLocales
  ): boolean {
    const localeUrlPart = path.split("/")[1];
    return !!(localeUrlPart && supportedLocales.find(v => v === localeUrlPart));
  }

  changeLocale(route: Route, targetLocale: string): string {
    const routeWithoutLocale = this.removeLocale(route, false);
    return routeWithoutLocale === ""
      ? `/${targetLocale}`
      : `/${targetLocale}/${routeWithoutLocale}`;
  }

  getFullLocalizedPath(route: Route | string, locale: string): string {
    if (typeof route === "object") {
      return this.getFullLocalizedPathFromString(
        (route as Route).fullPath,
        locale
      );
    } else {
      return this.getFullLocalizedPathFromString(route, locale);
    }
  }

  private getFullLocalizedPathFromString(path: string, locale: string): string {
    if (this.isLocalizedString(path)) {
      return path;
    } else {
      return path === "/" ? `/${locale}` : `/${locale}${path}`;
    }
  }

  removeLocale(
    route: Route | undefined,
    returnAbsoluteRoute = true
  ): string | null {
    if (route == null) {
      return null;
    } else {
      return this.supportedLocales.find(
        locale => locale == RouteLocalizer.getLocaleFromPath(route.fullPath)
      )
        ? RouteLocalizer.getPathWithoutLocale(route, returnAbsoluteRoute)
        : route.fullPath;
    }
  }

  private static getPathWithoutLocale(
    route: Route,
    returnAbsoluteRoute: boolean
  ) {
    return route.fullPath.length == 3 && returnAbsoluteRoute
      ? "/"
      : route.fullPath.slice(returnAbsoluteRoute ? 3 : 4);
  }

  private static getLocaleFromPath(path: string): string {
    return path.substring(1, 3);
  }
}
