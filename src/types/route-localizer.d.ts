import RouteLocalizer from "../router/route-localizer";
declare module "vue/types/vue" {
  interface Vue {
    $localizeRoutes: RouteLocalizer;
  }
}
