import Vue from "vue";
import VueI18n from "vue-i18n";
import { loadLocaleMessages } from "./locale-loader";
import { DEFAULT_LOCALE } from "./constants/locales";
import { Store } from "./store/store";

Vue.use(VueI18n);

const getI18n = (store: Store) => {
  return new VueI18n({
    locale:
      store.currentLocale || navigator.language.split("-")[0] || DEFAULT_LOCALE,
    fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || DEFAULT_LOCALE,
    messages: loadLocaleMessages()
  });
};

export default getI18n;
