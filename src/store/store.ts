import Vue from "vue";
import {
  DEFAULT_LOCALE,
  LOCAL_STORAGE_LOCALE_KEY,
  SUPPORTED_LOCALES
} from "../constants/locales";

export interface Store {
  menuOpen: boolean;
  accentColor?: string;
  currentLocale: string;
}

export interface Mutations {
  setAccentColor(color: string): void;
  setMenuOpen(menuOpen: boolean): void;
  setCurrentLocale(locale: string): void;
}

export function createStore(localStorage: Storage) {
  const store: Store = Vue.observable({
    accentColor: undefined,
    menuOpen: false,
    currentLocale:
      SUPPORTED_LOCALES.find(
        l => l === localStorage.getItem(LOCAL_STORAGE_LOCALE_KEY)
      ) || DEFAULT_LOCALE
  });

  const mutations: Mutations = {
    setAccentColor(color: string): void {
      store.accentColor = color;
    },
    setMenuOpen(menuOpen: boolean): void {
      store.menuOpen = menuOpen;
    },
    setCurrentLocale(locale: string) {
      store.currentLocale = locale;
      localStorage.setItem(LOCAL_STORAGE_LOCALE_KEY, locale);
    }
  };

  return { store, mutations };
}
