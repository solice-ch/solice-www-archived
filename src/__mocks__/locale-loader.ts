import enMessages from "../locales/en.json";
import { LocaleMessages } from "vue-i18n";

export function loadLocaleMessages(): LocaleMessages {
  return {
    en: enMessages
  };
}
