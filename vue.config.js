module.exports = {
  pluginOptions: {
    i18n: {
      locale: "de",
      fallbackLocale: "de",
      localeDir: "locales",
      enableInSFC: true
    }
  },
  chainWebpack: config => {
    config.module
      .rule("scss")
      .oneOf("vue")
      .use("resolve-url-loader")
      .loader("resolve-url-loader")
      .before("sass-loader")
      .end()
      .use("sass-loader")
      .loader("sass-loader")
      .tap(options => ({
        ...options,
        sourceMap: true
      }));
  }
};
