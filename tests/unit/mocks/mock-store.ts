import Vue from "vue";
import { Store } from "../../../src/store/store";

export function createMockStore(
  initialState: Store = {
    menuOpen: false,
    accentColor: undefined,
    currentLocale: "INVALID"
  }
) {
  const store: Store = Vue.observable(initialState);

  function setMenuOpen(menuOpen: boolean) {
    store.menuOpen = menuOpen;
  }

  function setAccentColor(accentColor: string) {
    store.accentColor = accentColor;
  }

  function setCurrentLocale(locale: string) {
    store.currentLocale = locale;
  }

  return Object.freeze({
    store,
    mutations: { setMenuOpen, setAccentColor, setCurrentLocale }
  });
}
