import { VueConstructor } from "vue";
import CompositionApi from "@vue/composition-api";
import Link from "../../src/components/Link.vue";
import { createLocalVue, shallowMount } from "@vue/test-utils";
import { createMockStore } from "./mocks/mock-store";

let localVue: VueConstructor;

const shallowMountLink = (
  props: object,
  slot?: string,
  getFullLocalizedPathMock = jest.fn(url => url),
  storeMock = createMockStore()
) => {
  return shallowMount(Link, {
    localVue,
    propsData: props,
    mocks: {
      $localizeRoutes: {
        getFullLocalizedPath: getFullLocalizedPathMock
      }
    },
    provide: { store: storeMock.store },
    stubs: ["router-link"],
    slots: {
      ...(slot
        ? {
            default: slot
          }
        : {})
    }
  });
};

beforeEach(() => {
  localVue = createLocalVue();
  localVue.use(CompositionApi);
});

describe("Link", () => {
  it("renders link with target _self but without href or text", () => {
    const wrapper = shallowMountLink({});

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("renders link with set target, href and text", () => {
    const wrapper = shallowMountLink(
      {
        url: "https://www.google.com",
        target: "_blank"
      },
      "Google"
    );

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("renders a router-link with url as to property and text for relative urls, target is omitted", () => {
    const wrapper = shallowMountLink(
      {
        url: "/about",
        target: "_blank"
      },
      "About Us"
    );

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("localizes url if url property is relative", () => {
    const getFullLocalizedPathStub = jest
      .fn()
      .mockImplementation(() => "localizedUrl");
    const wrapper = shallowMountLink(
      {
        url: "/about",
        target: "_blank"
      },
      "About Us",
      getFullLocalizedPathStub
    );

    const routerLink = wrapper.find("router-link-stub");

    expect(routerLink.attributes().to).toBe("localizedUrl");
  });

  it("does not localize url if url property is not relative", () => {
    const getFullLocalizedPathStub = jest
      .fn()
      .mockImplementation(() => "localizedUrl");
    const wrapper = shallowMountLink(
      {
        url: "https://www.google.com",
        target: "_blank"
      },
      "Google",
      getFullLocalizedPathStub
    );

    const routerLink = wrapper.find("a");

    expect(routerLink.attributes().href).toBe("https://www.google.com");
  });

  it("updates the localization of a localized link when the current locale gets changed", async () => {
    const storeMock = createMockStore();
    storeMock.mutations.setCurrentLocale("de");
    const getFullLocalizedPathSpy = jest
      .fn()
      .mockImplementation(() => `/${storeMock.store.currentLocale}/about`);
    const wrapper = shallowMountLink(
      {
        url: "/de/about",
        target: "_blank"
      },
      "About Us",
      getFullLocalizedPathSpy,
      storeMock
    );

    storeMock.mutations.setCurrentLocale("en");
    const routerLink = wrapper.find("router-link-stub");
    await wrapper.vm.$nextTick();

    expect(routerLink.attributes().to).toBe("/en/about");
  });
});
