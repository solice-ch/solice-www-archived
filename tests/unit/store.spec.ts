import { createStore, Mutations, Store } from "../../src/store/store";
import { LOCAL_STORAGE_LOCALE_KEY } from "../../src/constants/locales";

let storeContainer: { store: Store; mutations: Mutations };
beforeEach(function() {
  storeContainer = createStore(localStorage);
});

describe("store", function() {
  describe("setAccentColor", function() {
    it("returns undefined when setting any string", function() {
      expect(
        storeContainer.mutations.setAccentColor("#121212")
      ).toBeUndefined();
    });
    it("can be called multiple times", function() {
      storeContainer.mutations.setAccentColor("#121212");
      storeContainer.mutations.setAccentColor("#121212");
    });
  });

  describe("store.accentColor", function() {
    it("returns undefined without setting a value before", function() {
      expect(storeContainer.store.accentColor).toBe(undefined);
    });
    it("returns a value if one was set before", function() {
      storeContainer.mutations.setAccentColor("white");
      expect(storeContainer.store.accentColor).toBe("white");
    });
  });

  describe("setMenuOpen", function() {
    it("returns undefined when setting a boolean value", function() {
      expect(storeContainer.mutations.setMenuOpen(true)).toBeUndefined();
    });
    it("can be called multiple times", function() {
      storeContainer.mutations.setMenuOpen(true);
      storeContainer.mutations.setMenuOpen(true);
    });
  });

  describe("store.menuOpen", function() {
    it("returns undefined without setting a value before", function() {
      expect(storeContainer.store.accentColor).toBe(undefined);
    });
    it("returns a value if one was set before", function() {
      storeContainer.mutations.setMenuOpen(true);
      expect(storeContainer.store.menuOpen).toBe(true);
    });
  });

  describe("setCurrentLocale", function() {
    it("returns undefined when setting a string value", function() {
      expect(storeContainer.mutations.setCurrentLocale("de")).toBeUndefined();
    });
    it("stores the set locale in the localStorage", function() {
      const storeContainer = createStore(localStorage);

      storeContainer.mutations.setCurrentLocale("de");

      expect(localStorage.getItem(LOCAL_STORAGE_LOCALE_KEY)).toBe("de");
    });
    it("can be called multiple times", function() {
      storeContainer.mutations.setCurrentLocale("de");
      storeContainer.mutations.setCurrentLocale("de");
    });
  });

  describe("store.currentLocale", function() {
    it("returns de without setting a value before and no locale set in the localStorage", function() {
      expect(storeContainer.store.currentLocale).toBe("de");
    });
    it("returns en without setting a value before and with en persisted in the localStorage", function() {
      localStorage.setItem(LOCAL_STORAGE_LOCALE_KEY, "en");
      const storeContainer = createStore(localStorage);

      expect(storeContainer.store.currentLocale).toBe("en");
    });
    it("returns the default locale if the locale in the localStorage is not supported", function() {
      localStorage.setItem(LOCAL_STORAGE_LOCALE_KEY, "ar");
      const storeContainer = createStore(localStorage);

      expect(storeContainer.store.currentLocale).toBe("de");
    });
    it("returns a value if one was set before", function() {
      storeContainer.mutations.setCurrentLocale("de");
      expect(storeContainer.store.currentLocale).toBe("de");
    });
  });
});
