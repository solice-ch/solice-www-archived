import RouteLocalizer from "../../src/router/route-localizer";
import VueI18n from "vue-i18n";
import Vue from "vue";
import { Route } from "vue-router";

class StubRoute implements Route {
  fullPath: string;
  hash: never;
  matched: never;
  params: never;
  path: never;
  query: never;

  constructor(fullPath: string) {
    this.fullPath = fullPath;
  }
}

beforeAll(() => Vue.use(VueI18n));

describe("RouteLocalizer", () => {
  describe("getLocaleRegexString", () => {
    it("returns regex that matches each of the supported locales", function() {
      const sut = new RouteLocalizer(["af", "al", "as"]);
      expect(sut.getLocaleRegexString()).toEqual("(af|al|as)");
    });
    it("returns regex that matches each of the supported locales", function() {
      const sut = new RouteLocalizer(["af"]);
      expect(sut.getLocaleRegexString()).toEqual("(af)");
    });
    it("without supported locales returns regex that never matches anything", function() {
      const sut = new RouteLocalizer([]);
      expect(sut.getLocaleRegexString()).toEqual("(?=a)b");
    });
  });

  describe("isLocalized", () => {
    it("returns false with a root route", function() {
      const sut = new RouteLocalizer([]);
      expect(sut.isLocalized(new StubRoute("/"))).toBe(false);
    });
    it("returns false with a root route path", function() {
      const sut = new RouteLocalizer([]);
      expect(sut.isLocalized("/")).toBe(false);
    });
    it("returns false with a locale but no supported locales", function() {
      const sut = new RouteLocalizer([]);
      expect(sut.isLocalized(new StubRoute("/en"))).toBe(false);
    });
    it("returns false with a locale that is not specified in the supported locales", function() {
      const sut = new RouteLocalizer(["en"]);
      expect(sut.isLocalized(new StubRoute("/fr"))).toBe(false);
    });
    it("returns true with a locale that is specified in the supported locales", function() {
      const sut = new RouteLocalizer(["ar", "en", "af"]);
      expect(sut.isLocalized(new StubRoute("/en"))).toBe(true);
    });
  });

  describe("getFullLocalizedPath", () => {
    it("with a root path returns the localized root path with the specified locale", function() {
      const sut = new RouteLocalizer(["en"]);
      expect(sut.getFullLocalizedPath(new StubRoute("/"), "en")).toEqual("/en");
    });
    it("with a root path string returns the localized root path with the specified locale", function() {
      const sut = new RouteLocalizer(["en"]);
      expect(sut.getFullLocalizedPath("/", "en")).toEqual("/en");
    });
    it("with a path that identifies a sub resource returns a path that contains the sub resource", function() {
      const sut = new RouteLocalizer(["en"]);
      expect(
        sut.getFullLocalizedPath(new StubRoute("/development"), "en")
      ).toEqual("/en/development");
    });
    it("returns the unmodified route if it is already localized", function() {
      const sut = new RouteLocalizer(["en"]);
      expect(sut.getFullLocalizedPath(new StubRoute("/en"), "en")).toEqual(
        "/en"
      );
    });
  });

  describe("changeLocale", () => {
    it("changes locale of root path with a target locale that is different that the current locale", function() {
      const sut = new RouteLocalizer(["de", "en"]);
      expect(sut.changeLocale(new StubRoute("/en"), "de")).toEqual("/de");
    });
    it("changes locale of a path with a target locale that is different that the current locale", function() {
      const sut = new RouteLocalizer(["de", "en"]);
      expect(sut.changeLocale(new StubRoute("/en/a/b"), "de")).toEqual(
        "/de/a/b"
      );
    });
    it("has no effect on url a that already is in the target localization", function() {
      const sut = new RouteLocalizer(["de", "en"]);
      expect(sut.changeLocale(new StubRoute("/en"), "en")).toEqual("/en");
    });
  });

  describe("removeLocale", () => {
    it("removes the locale from a path if the locale is known", () => {
      const sut = new RouteLocalizer(["en"]);
      expect(sut.removeLocale(new StubRoute("/en/a"))).toBe("/a");
    });

    it("removes the locale from a root path and returns an absolute path if specified and the locale is known", () => {
      const sut = new RouteLocalizer(["en"]);
      expect(sut.removeLocale(new StubRoute("/en"))).toBe("/");
    });

    it("removes the locale from a nested path if the locale is known", () => {
      const sut = new RouteLocalizer(["en"]);
      expect(sut.removeLocale(new StubRoute("/en/a/b"))).toBe("/a/b");
    });

    it("returns a relative path with returnAbsolutePath = false", () => {
      const sut = new RouteLocalizer(["en"]);
      expect(sut.removeLocale(new StubRoute("a"), false)).toBe("a");
    });

    it("does not remove the locale from a path if the locale is unknown", () => {
      const sut = new RouteLocalizer(["de"]);
      expect(sut.removeLocale(new StubRoute("/en/a"))).toBe("/en/a");
    });

    it("returns an empty string with a route that has an empty path", () => {
      const sut = new RouteLocalizer(["de"]);
      expect(sut.removeLocale(new StubRoute(""))).toBe("");
    });

    it("returns null for a route that is null or undefined", () => {
      const sut = new RouteLocalizer(["de"]);
      expect(sut.removeLocale(undefined)).toBe(null);
    });
  });
});
