import Vue from "vue";
import VueCompositionApi from "@vue/composition-api";
import LanguageChanger from "../../src/components/LanguageChanger.vue";
import { shallowMount } from "@vue/test-utils";
import { createMockStore } from "./mocks/mock-store";

jest.mock("../../src/locale-loader");

const getShallowMountedComponent = (
  currentLocale: string,
  mocks = {},
  storeMock = createMockStore()
) =>
  shallowMount(LanguageChanger, {
    provide: {
      i18n: { locale: currentLocale },
      store: storeMock.store,
      mutations: storeMock.mutations
    },
    mocks: {
      ...mocks,
      $t: (key: string) => key
    }
  });

function getStubs() {
  return {
    $router: {
      replace: () => null
    },
    $localizeRoutes: {
      changeLocale: () => null
    }
  };
}

describe("LanguageChanger", () => {
  Vue.use(VueCompositionApi);

  it("displays EN when the current locale is de", () => {
    expect(getShallowMountedComponent("de").text()).toBe("EN");
  });

  it("displays DE when the current locale is en", () => {
    expect(getShallowMountedComponent("de").text()).toBe("EN");
  });

  it("displays EN when the current locale is anything but de", () => {
    expect(getShallowMountedComponent("ar").text()).toBe("EN");
  });

  it("displays DE when the current locale is de and the button is clicked", async () => {
    const sut = getShallowMountedComponent("de", getStubs());
    sut.element.innerText = "EN";
    await sut.trigger("click");
    expect(sut.text()).toBe("DE");
  });

  it("displays EN when the current locale is en and the button is clicked", async () => {
    const sut = getShallowMountedComponent("en", getStubs());
    sut.element.innerText = "DE";

    await sut.trigger("click");

    expect(sut.text()).toBe("EN");
  });

  it("does nothing when the inner text of the buttons is neither EN or DE", async () => {
    const sut = getShallowMountedComponent("de", getStubs());
    sut.element.innerText = "AR";

    await sut.trigger("click");

    expect(sut.text()).toBe("EN");
  });

  it("updates the store with the new language when clicking the toggle", async () => {
    const storeMock = createMockStore();
    const sut = getShallowMountedComponent("de", getStubs(), storeMock);
    sut.element.innerText = "EN";

    await sut.trigger("click");

    expect(storeMock.store.currentLocale).toBe("en");
  });

  it("changes the url when clicked", async () => {
    const mocks = {
      $router: {
        replace: jest.fn(),
        currentRoute: "currentRoute"
      },
      $localizeRoutes: {
        changeLocale: jest.fn(() => "changedLocale")
      }
    };
    const sut = getShallowMountedComponent("de", mocks);
    sut.element.innerText = "DE";

    await sut.trigger("click");

    expect(mocks.$router.replace).toBeCalledWith("changedLocale");
    expect(mocks.$localizeRoutes.changeLocale).toBeCalledWith(
      "currentRoute",
      "de"
    );
  });
});
