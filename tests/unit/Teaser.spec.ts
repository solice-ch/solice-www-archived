import Vue from "vue";
import CompositionApi from "@vue/composition-api";
import Teaser from "../../src/components/Teaser.vue";
import { shallowMount } from "@vue/test-utils";
import CustomLink from "@/components/Link.vue";
import { createMockStore } from "./mocks/mock-store";

const getShallowMountedComponent = (
  props = {},
  mocks = {},
  mockStore = createMockStore()
) =>
  shallowMount(Teaser, {
    propsData: props,
    mocks,
    provide: { mutations: mockStore.mutations }
  });

const getStubs = () => ({
  $localizeRoutes: {
    getFullLocalizedPath: jest.fn()
  }
});

describe("Teaser", () => {
  Vue.use(CompositionApi);

  it("renders empty teaser with no properties set", () => {
    const wrapper = getShallowMountedComponent(
      {
        teaserItems: []
      },
      getStubs()
    );

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("renders teaser with set background, title and leadText", () => {
    const wrapper = getShallowMountedComponent({
      backgroundColor: "#FFFFFF",
      title: "Lorem Ipsum",
      teaserItems: [
        {
          text:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        }
      ]
    });

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("renders teaser and link with all their properties correctly", () => {
    const wrapper = getShallowMountedComponent({
      backgroundColor: "#FFFFFF",
      title: "Lorem Ipsum",
      teaserItems: [
        {
          text:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
          link: {
            text: "Solice",
            target: "_blank",
            url: "https://www.solice.ch"
          }
        }
      ]
    });

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("does not render a link within if no link property is set", () => {
    const wrapper = getShallowMountedComponent({
      backgroundColor: "#FFFFFF",
      title: "Lorem Ipsum",
      teaserItems: [
        {
          text:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        }
      ]
    });

    expect(wrapper.contains("link")).toBe(false);
  });

  it("sets the accent color when clicking a teaser link", () => {
    const storeMock = createMockStore();
    const wrapper = getShallowMountedComponent(
      {
        backgroundColor: "#121212",
        teaserItems: [
          {
            link: {
              text: "Solice",
              target: "_blank",
              url: "https://www.solice.ch"
            }
          }
        ]
      },
      {},
      storeMock
    );

    const teaserLink = wrapper.find(CustomLink);
    teaserLink.trigger("click");

    expect(storeMock.store.accentColor).toBe("#121212");
  });

  it("renders all teaser items as specified", () => {
    const wrapper = getShallowMountedComponent({
      backgroundColor: "#FFFFFF",
      title: "Lorem Ipsum",
      teaserItems: [
        {
          text: "Lorem ipsum"
        },
        {
          text: "dolor sit amet"
        },
        {
          text: "consectetur adipiscing elit"
        }
      ]
    });

    expect(wrapper.findAll(".teaser__item").length).toBe(3);
  });
});
