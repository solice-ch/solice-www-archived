import { createLocalVue, shallowMount } from "@vue/test-utils";
import App from "../../src/App.vue";
import VueCompositionApi from "@vue/composition-api";
import { VueConstructor } from "vue";
import { createMockStore } from "./mocks/mock-store";
import { Mutations, Store } from "../../src/store/store";

jest.mock("../../src/locale-loader");

let localVue: VueConstructor;

beforeEach(() => {
  localVue = createLocalVue();
  localVue.use(VueCompositionApi);
});

function getShallowMountedApp(
  localVue: VueConstructor,
  i18nStub: { locale: string },
  storeMock: { store: Store; mutations: Mutations }
) {
  return shallowMount(App, {
    stubs: ["router-view"],
    localVue,
    propsData: {
      window: null,
      document: null,
      store: storeMock.store,
      mutations: storeMock.mutations,
      i18n: i18nStub
    }
  });
}

describe("App", function() {
  it("sets the language in the store after it is mounted", function() {
    const storeMock = createMockStore();
    getShallowMountedApp(localVue, { locale: "en" }, storeMock);

    expect(storeMock.store.currentLocale).toBe("en");
  });
});
