import { createLocalVue, shallowMount } from "@vue/test-utils";
import VueCompositionApi from "@vue/composition-api";
import NavigationItem from "../../src/components/NavigationItem.vue";
import { VueConstructor } from "vue";

let localVue: VueConstructor;

beforeEach(() => {
  localVue = createLocalVue();
  localVue.use(VueCompositionApi);
});

describe("NavigationItem", function() {
  it("renders navigation item passing properties and slot to CustomLink", function() {
    const sut = shallowMount(NavigationItem, {
      localVue,
      propsData: {
        to: "/about"
      },
      slots: {
        default: "About us"
      }
    });

    expect(sut.html()).toMatchSnapshot();
  });
});
