import Vue from "vue";
import VueCompositionApi from "@vue/composition-api";
import { shallowMount } from "@vue/test-utils";
import CloseSidebar from "../../src/components/CloseSidebar.vue";
import { createMockStore } from "./mocks/mock-store";

const getShallowMountedComponent = (
  mocks = {},
  mockStore = createMockStore().store
) =>
  shallowMount(CloseSidebar, {
    mocks,
    provide: { store: mockStore }
  });

describe("CloseSidebar", () => {
  Vue.use(VueCompositionApi);

  it("is not shown for route with no sidebar meta property", () => {
    const mocks = {
      $route: {
        meta: {}
      }
    };
    const wrapper = getShallowMountedComponent(mocks);

    expect(wrapper.findAll(".sidebar").length).toBe(0);
  });

  it("is shown for route with truthy sidebar meta property", () => {
    const mocks = {
      $route: {
        meta: { sidebar: true }
      }
    };
    const wrapper = getShallowMountedComponent(mocks);

    expect(wrapper.findAll(".sidebar").length).toBe(1);
  });

  it("sets no background color if accent color is not set in the store", () => {
    const mocks = {
      $route: {
        meta: { sidebar: true }
      }
    };
    const mockStore = createMockStore({ menuOpen: false, currentLocale: "" });
    const wrapper = getShallowMountedComponent(mocks, mockStore.store);

    expect(wrapper.element.style.backgroundColor).toBe("");
  });

  it("sets background color if accent color is set in the store", () => {
    const mocks = {
      $route: {
        meta: { sidebar: true }
      }
    };
    const mockStore = createMockStore({
      accentColor: "#000",
      menuOpen: false,
      currentLocale: ""
    });
    const wrapper = getShallowMountedComponent(mocks, mockStore.store);

    expect(wrapper.element.style.backgroundColor).toBe("rgb(0, 0, 0)");
  });

  it("triggers router back", async () => {
    const mocks = {
      $router: {
        go: jest.fn()
      },
      $route: {
        meta: { sidebar: true }
      }
    };
    const wrapper = getShallowMountedComponent(mocks);

    await wrapper.find(".icon-close").trigger("click");

    expect(mocks.$router.go).toBeCalledWith(-1);
  });

  it("stays closed if it is disabled and the mobile menu is open", () => {
    const mocks = {
      $route: {
        meta: { sidebar: false }
      }
    };
    const mockStore = createMockStore({ menuOpen: true, currentLocale: "" });
    const wrapper = getShallowMountedComponent(mocks, mockStore.store);

    expect(wrapper.find(".sidebar").exists()).toBe(false);
  });

  it("does not open if the mobile menu is open and it is enabled", () => {
    const mocks = {
      $route: {
        meta: { sidebar: true }
      }
    };
    const mockStore = createMockStore({ menuOpen: true, currentLocale: "" });
    const wrapper = getShallowMountedComponent(mocks, mockStore.store);

    expect(wrapper.find(".sidebar").exists()).toBe(false);
  });

  it("opens if the mobile menu is closed and it is enabled", () => {
    const mocks = {
      $route: {
        meta: { sidebar: true }
      }
    };
    const mockStore = createMockStore({ menuOpen: false, currentLocale: "" });
    const wrapper = getShallowMountedComponent(mocks, mockStore.store);

    expect(wrapper.find(".sidebar").exists()).toBe(true);
  });

  it("closes if it is enabled and the menu gets opened", async () => {
    const mocks = {
      $route: {
        meta: { sidebar: true }
      }
    };
    const mockStore = createMockStore({ menuOpen: false, currentLocale: "" });
    const wrapper = getShallowMountedComponent(mocks, mockStore.store);

    mockStore.mutations.setMenuOpen(true);
    await wrapper.vm.$nextTick();

    expect(wrapper.find(".sidebar").exists()).toBe(false);
  });
});
