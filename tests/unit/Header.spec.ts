import VueCompositionApi from "@vue/composition-api";
import { createLocalVue, shallowMount, Wrapper } from "@vue/test-utils";
import Header from "../../src/components/Header.vue";
import { VueConstructor } from "vue";
import NavigationItem from "../../src/components/NavigationItem.vue";
import LanguageChanger from "../../src/components/LanguageChanger.vue";
import { createMockStore } from "./mocks/mock-store";

jest.mock("../../src/locale-loader");

interface HeaderWrapper extends Wrapper<Vue> {
  openSmallViewportMenu: () => void;
  closeSmallViewportMenu: () => void;
}

function getWindow(scrollToMock = () => null) {
  return Object.assign(global, {
    scrollTo: scrollToMock
  });
}

const getMountedComponent = (
  localVue: VueConstructor,
  window = getWindow(),
  mutations = createMockStore().mutations
) => {
  const sut = (shallowMount(Header, {
    localVue,
    propsData: {
      window: window,
      document
    },
    mocks: {
      $t: (key: string) => key
    },
    provide: { mutations: mutations },
    stubs: ["router-link"]
  }) as unknown) as HeaderWrapper;

  sut.openSmallViewportMenu = async function() {
    this.setData({ isSmallViewportMenuOpen: true });
    await sut.vm.$nextTick();
  };

  sut.closeSmallViewportMenu = async function() {
    this.setData({ isSmallViewportMenuOpen: false });
    await sut.vm.$nextTick();
  };
  return sut;
};

let localVue: VueConstructor;

beforeEach(() => {
  localVue = createLocalVue();
  localVue.use(VueCompositionApi);
});

function enforceJsdomRestore() {
  document.getElementsByTagName("html")[0].innerHTML = "";
}

describe("Header", () => {
  it("displays the navigation after clicking the small viewport menu button", async () => {
    const sut = getMountedComponent(localVue);

    sut.find(".nav__menu-toggle").trigger("click");
    await sut.vm.$nextTick();

    expect(sut.find("nav").element.classList).toContain("nav--open");
  });

  it("sets overflow hidden on the body element when the small viewport menu is open", async () => {
    const sut = getMountedComponent(localVue);

    await sut.openSmallViewportMenu();

    const body = document.getElementsByTagName("body").item(0);
    expect(body?.style.overflow).toBe("hidden");
  });

  it("removes overflow hidden on the body element when the small viewport menu is closed", async () => {
    const sut = getMountedComponent(localVue);

    await sut.closeSmallViewportMenu();

    const body = document.getElementsByTagName("body").item(0);
    expect(body?.style.overflow).toBe("");
  });

  it("does not throw if no body element exists", async () => {
    const sut = getMountedComponent(localVue);
    const body = document.getElementsByTagName("body").item(0);
    if (body) {
      body.parentNode?.removeChild(body);
    }

    await expect(sut.openSmallViewportMenu()).resolves.not.toThrow();

    enforceJsdomRestore();
  });

  it("has no navigation after clicking the small viewport menu button twice", async () => {
    const sut = getMountedComponent(localVue);

    sut.find(".nav__menu-toggle").trigger("click");
    sut.find(".nav__menu-toggle").trigger("click");
    await sut.vm.$nextTick();

    expect(sut.find("nav").element.classList).not.toContain("nav--open");
  });

  it("hides the navigation if any navigation item gets clicked", async () => {
    const sut = getMountedComponent(localVue);

    await sut.openSmallViewportMenu();
    sut.find(NavigationItem).trigger("click");
    await sut.vm.$nextTick();

    expect(sut.find("nav").element.classList).not.toContain("nav--open");
  });

  it("does not hide the navigation if the language toggle gets clicked", async () => {
    const sut = getMountedComponent(localVue);

    await sut.openSmallViewportMenu();
    sut.find(LanguageChanger).trigger("click");
    await sut.vm.$nextTick();

    expect(sut.find("nav").element.classList).toContain("nav--open");
  });

  it("renders the menu button without the open class if the menu is closed", async () => {
    const sut = getMountedComponent(localVue);
    await sut.vm.$nextTick();

    const menuButton = sut.find(".nav__menu-toggle");
    expect(menuButton.classes()).not.toContain("nav__menu-toggle--open");
  });

  it("renders the menu button with the open class if the menu is open", async () => {
    const sut = getMountedComponent(localVue);

    await sut.openSmallViewportMenu();

    const menuButton = sut.find(".nav__menu-toggle");
    expect(menuButton.classes()).toContain("nav__menu-toggle--open");
  });

  it("sets menuOpen to false in the store when initialized", () => {
    const mockStore = createMockStore();
    getMountedComponent(localVue, getWindow(), mockStore.mutations);

    expect(mockStore.store.menuOpen).toBe(false);
  });

  it("sets menuOpen to true in the store when the menu gets opened", async () => {
    const mockStore = createMockStore();
    const sut = getMountedComponent(localVue, getWindow(), mockStore.mutations);

    await sut.openSmallViewportMenu();

    expect(mockStore.store.menuOpen).toBe(true);
  });

  it("it resets windows scroll position to point 0,0 when the mobile menu opens", async () => {
    const scrollToSpy = jest.fn();
    const sut = getMountedComponent(localVue, getWindow(scrollToSpy));

    sut.openSmallViewportMenu();
    await sut.vm.$nextTick();

    expect(scrollToSpy).toHaveBeenCalledWith(0, 0);
  });
});
