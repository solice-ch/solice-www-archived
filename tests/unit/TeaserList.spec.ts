import Vue from "vue";
import CompositionApi from "@vue/composition-api";
import TeaserList from "../../src/components/TeaserList.vue";
import { shallowMount } from "@vue/test-utils";

describe("TeaserList", () => {
  Vue.use(CompositionApi);
  it("renders empty teaser list with no properties set", () => {
    const wrapper = shallowMount(TeaserList, { propsData: {} });

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("renders teaser component for each teaser array element", () => {
    const wrapper = shallowMount(TeaserList, {
      propsData: {
        teasers: [
          {
            id: 0,
            title: "Title 0",
            teaserItems: [
              {
                text: "Leadtext 0",
                link: {
                  text: "Link 0",
                  target: "_blank",
                  url: "/"
                }
              }
            ]
          },
          {
            id: 1,
            title: "Title 1",
            teaserItems: [
              {
                text: "Leadtext 1",
                link: {
                  text: "Link 1",
                  target: "_blank",
                  url: "/"
                }
              }
            ]
          }
        ]
      }
    });

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("renders teasers as teaser--small and sets teaserlist--small class if teaser list variation is small and not explicitly set for teaser", () => {
    const wrapper = shallowMount(TeaserList, {
      propsData: {
        teasers: [
          {
            id: 0,
            title: "Title 0",
            teaserItems: [
              {
                text: "Leadtext 0",
                link: {
                  text: "Link 0",
                  target: "_blank",
                  url: "/"
                }
              }
            ]
          },
          {
            id: 1,
            title: "Title 1",
            teaserItems: [
              {
                text: "Leadtext 1",
                link: {
                  text: "Link 1",
                  target: "_blank",
                  url: "/"
                }
              }
            ]
          },
          {
            id: 2,
            title: "Title 2",
            teaserItems: [
              {
                text: "Leadtext 2",
                link: {
                  text: "Link 2",
                  target: "_blank",
                  url: "/"
                }
              }
            ],
            variation: "teaser--big"
          }
        ],
        variation: "small"
      }
    });

    expect(wrapper.html()).toMatchSnapshot();
  });
});
