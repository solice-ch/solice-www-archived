# solice-www-archived

This project is the archived Solice website. Unfortunately, as we closed shop while the website redesign was in progress, we didn't get to finalize and translate some page content.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test
```

### Lints and fixes files
```
yarn lint --fix
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Deployment

In the .build folder you can find a Dockerfile and docker-compose.yml used to build a container. You may edit the docker-compose.yml and .env file to change port configurations.
Simply run `docker-compose up -d` to start the container.

We are using GitLabs container registry to host our images and used to deploy the containers on our servers using Ansible.